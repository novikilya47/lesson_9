const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const autopopulate = require("mongoose-autopopulate");
 
const AdminScheme = new Schema(
    {adminName: String, 
    adminLogin: String, 
    adminPassword: String, 
    role: {type : String, default : "admin"},
    room:[{type:mongoose.Schema.Types.ObjectId, ref: "room", autopopulate: true}]},
    { versionKey: false });
    AdminScheme.plugin(require("mongoose-autopopulate"));

const Admin = mongoose.model("admin", AdminScheme); 
module.exports = Admin;
  
