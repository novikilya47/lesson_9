const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const autopopulate = require("mongoose-autopopulate");
 
const RoomScheme = new Schema(
    {roomNumber: Number, 
    roomUsers: {type : mongoose.Schema.Types.ObjectId, ref: "room", autopopulate: true}, 
    roomAdmin: {type : mongoose.Schema.Types.ObjectId, ref: "room", autopopulate: true}
    },
    { versionKey: false });

const Room = mongoose.model("room", RoomScheme); 
module.exports = Room;