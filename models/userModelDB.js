const mongoose = require("mongoose");
const Schema = mongoose.Schema;
 
const UserScheme = new Schema(
    {userName: String, 
    userLogin: String, 
    userPassword: String, role: {type : String, default : "user"}, 
    room:[{type:mongoose.Schema.Types.ObjectId, ref: "room", autopopulate: true}]
    },
    { versionKey: false });

    UserScheme.plugin(require("mongoose-autopopulate"));

const User = mongoose.model("User", UserScheme); 
module.exports = User;
