const express = require('express');
const app = express();
const user = require('./routes/user');
const admin = require('./routes/admin');
const auth = require("./routes/auth");
const room = require("./routes/room");
const mongoose = require("mongoose");

app.use("/user", user);
app.use("/admin", admin);
app.use("/auth", auth);
app.use("/room", room);

mongoose.connect("mongodb://localhost:27017/BD", { useUnifiedTopology: true, useNewUrlParser:true}, function(err){
    if(err) {return console.log('Something broke!');}
    app.listen(3000, function(){
        console.log("Сервер работает");
    });
});


