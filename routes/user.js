const express = require('express');
const user = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const userController = require("../controllers/userControllers");

user.post("/create", urlencodedParser, userController.postData);

user.get("/:id", userController.getData);

user.delete("/:id", userController.deleteData);

user.put("/:id", urlencodedParser, userController.putData);

module.exports = user;
