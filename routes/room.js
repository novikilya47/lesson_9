const express = require('express');
const room = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const roomController = require("../controllers/roomControllers");

room.post("/create", urlencodedParser, roomController.authJWT, roomController.checkRole, roomController.createRoom);
room.delete("/:id", roomController.authJWT, roomController.checkRole, roomController.deleteRoom);
room.put("/:id", roomController.authJWT, roomController.checkRole, roomController.updateRoom);
room.get("/get", roomController.authJWT, roomController.getRoomsList);
room.get("/:id", roomController.authJWT, roomController.getRoomById);

module.exports = room;