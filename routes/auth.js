const express = require('express');
const auth = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const authControllers = require("../controllers/authControllers");
const room = require("../routes/room");

auth.post('/login', urlencodedParser, authControllers.auth, room);

module.exports = auth;