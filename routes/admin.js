const express = require('express');
const administrator = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const adminController = require("../controllers/adminControllers");



administrator.post("/create", urlencodedParser, adminController.postData);

administrator.get("/:id", adminController.getData);

administrator.delete("/:id", adminController.deleteData);

administrator.put("/:id", urlencodedParser, adminController.putData);

module.exports = administrator;
