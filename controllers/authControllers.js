const jwt = require('jsonwebtoken');
const express = require('express');
const auten = express.Router();
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';
const User = require("../models/userModelDB");
const Admin = require("../models/adminModelDB");

exports.auth = async function(req, res){
    const user = await User.findOne({userLogin:req.body.login, userPassword:req.body.password});
    const admin = await Admin.findOne({adminLogin:req.body.login, adminPassword:req.body.password});
    if (user){
        const accessToken = jwt.sign({login: user.userLogin, role: user.role, _id:user._id}, accessTokenSecret);
        res.json({accessToken});
    } else if (admin){
        const accessToken = jwt.sign({login: admin.adminLogin, role: admin.role, _id: admin._id}, accessTokenSecret);
        res.json({accessToken});
    } else{
        res.send('Incorrect login');
    }
};
