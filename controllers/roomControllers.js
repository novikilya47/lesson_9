const Room = require("../models/roomModelDB");
const Admin = require("../models/adminModelDB");
const User = require('../models/userModelDB');
const jwt = require('jsonwebtoken');
const { request, response } = require("express");
const { JsonWebTokenError } = require("jsonwebtoken");
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';

exports.authJWT = function (request, response, next){
    const authHeader = request.headers.authorization;
    if(authHeader){
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user)=>{
            if (err){
                return response.send("Неверный токен");
            }
            request.user = user;
            next();
        });
    } else{
        response.send("Не проведена авторизация");
    }
};

exports.checkRole = function (request, response, next){
    if (request.user.role !== "admin"){
        return response.send("Доступ запрещен");    
    }
    next();
};

exports.createRoom = async function (request, response){
    let newRoom = new Room({roomNumber: request.body.number, roomUsers:request.body.users});
    let adminOfRoom = await Admin.findOne({_id:request.user._id});
    newRoom.roomAdmin = request.user._id;
    await newRoom.save();
    adminOfRoom.room.push(newRoom._id);
    await adminOfRoom.save();
    response.send(`Создана комната номер ${newRoom.roomNumber}`);
};

exports.deleteRoom = async function (request, response){
    let room = await Room.findOne({_id:request.params.id});
    let roomAdmin = await Admin.findOne({_id:request.user._id});
    if (room && roomAdmin._id.toString() == room.roomAdmin._id.toString()){
        Room.deleteOne({_id:request.params.id}, function(err, result){
            if (err) {return response.send("ошибка");}
        }); 
        for (let i = 0; i < roomAdmin.room.length; i++){
            if (roomAdmin.room[i]._id.toString() == room._id.toString()){
                roomAdmin.room.splice(roomAdmin.room.indexOf(i),1);
                await roomAdmin.save();
            }
        }
        response.send("Комната удалена");          
    } else {
        response.send("Комната вам не принадлежит");
    }
};

exports.updateRoom = async function (request, response){
    let room = await Room.findOne({_id:request.params.id});
    let roomAdmin = await Admin.findOne({_id:request.user._id});
    if (room && roomAdmin._id.toString() == room.roomAdmin._id.toString()){
        Room.updateOne({_id:request.params.id},{roomNumber:15},function(err, result){
            if (err) {return response.send("ошибка");}
            response.send(`Номер комнаты изменен на ${room.roomNumber}`);
        });
    } else {
        response.send("Комната вам не принадлежит");
    }
};

exports.getRoomsList = async function (request, response){
    if (request.user.role == "admin"){
        let admin = await Admin.findOne({_id:request.user._id});
        response.send (`Вами созданы комнаты ${admin.room}`);
    } else {
        let user = await User.findOne({_id:request.user._id});
        response.send (`Вы состоите в комнатах ${user.room}`);
    }
};

exports.getRoomById = async function (request, response){
    if (request.user.role == "admin"){
        let room = await Room.findOne({_id:request.params.id});
        response.send (`${room}`);
    } else {
        let user = await User.findOne({_id:request.user._id});
        let arr = user.rooms;
        let find = request.params.id;
        response.send (`Вы состоите в комнатах ${user.room}`);
        
    }
};