const Admin = require("../models/adminModelDB");
 
exports.postData = function(request, response){
    Admin.create({adminName: request.body.adminName, adminLogin:request.body.adminLogin, adminPassword:request.body.adminPassword}, function(err, doc){
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные отправлены. ${doc} <h3>`); 
    });      
};

exports.getData = function(request, response){
    Admin.find({adminLogin:request.params.id}, function(err, doc){   
        if (err) {return console.log(err);}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

exports.deleteData = function(request, response){
    Admin.deleteOne({adminLogin:request.params.id}, function (err, result){
        if (err) {return console.log(err);}
        response.send(`<h3> Данные удалены </h3>`); 
    });       
};

exports.putData = function(request, response){
    Admin.updateOne({adminLogin:request.params.id}, {adminName: request.body.adminName}, function(err, result){
        if (err) {return console.log(err);}
        response.send(`<h3> Данные обновлены <h3>`);
    });        
};