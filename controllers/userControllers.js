const User = require("../models/userModelDB");
 
exports.postData = function(request, response){
    User.create({userName:request.body.userName, userLogin:request.body.userLogin, userPassword:request.body.userPassword}, function(err, doc){     
        if (err) {return console.log(err);}
        response.send(`<h3> Данные отправлены. ${doc} <h3>`); 
    });      
};

exports.getData = function(request, response){
    User.find({userLogin:request.params.id}, function(err, doc){           
        if (err) {return console.log(err);}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

exports.deleteData = function(request, response){
    User.deleteOne({userLogin:request.params.id}, function (err, result){      
        if (err) {return console.log(err);}
        response.send(`<h3> Данные удалены</h3>`); 
    });       
};

exports.putData = function(request, response){
    User.updateOne({userLogin:request.params.id}, {userName:request.body.userName}, function(err, result){
        if (err) {return console.log(err);}
        response.send(`<h3> Данные обновлены <h3>`);
    });        
};